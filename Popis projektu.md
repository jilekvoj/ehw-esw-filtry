<h1>Filtry</h1>

<h3>Team: Vojtěch Jílek, Karel Hevessy</h3>
<h3>Leader: Vojtěch Jílek </h3>

<h2>Zadání</h2>
Navrhněte a realizujte filtry typu dolní, horní a pásmová propust v audio pásmu se vzorkovacími
frekvencemi v rozsahu 8000Hz až 44100Hz. Filtry musí být implementovány minimálně pro dvě vzorkovací
frekvence dle možností implementační platformy a vždy po dohodě s vyučujícím.
Pro návrh koeficientů filtru použijte MATLAB. S pomocí osciloskopu a signálového generátoru ověřte funkci filtrů. Pro implementace v jazyce C aplikujte také VALGRIND. U filtrů a jejich
rozhraní změřte (získejte z simulace nebo syntézního nástroje) časové parametry implementace filtru a
zhodnoťte zda vyhovují zpracování signálu v reálném čase.

<h2>Způsob implementace a platforma</h2>
Raspberrry PI, filtr jako Linux executable, program v jazyce C.

Raspberrry PI, filtr jako exe aplikace pro Windows, program v jazyce C.

<h2>Parametry filtrů</h2>

| ID  | Typ | A | B | f0 | f1 | f2 | f3 
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 1   | Dolní propoust | -3 dB | -46dB | 100  | 150  | - | - 
| 2   | Dolní propoust | -3 dB | -46dB | 200  | 250  | - | - 
| 3   | Dolní propoust | -3 dB | -46dB | 500  | 550  | - | - 
| 4   | Dolní propoust | -3 dB | -46dB | 800  | 850  | - | - 
| 5   | Dolní propoust | -3 dB | -46dB | 1000 | 1100 | - | - 
| 6   | Horní propoust | -3 dB | -46dB | 250  | 300  | - | - 
| 8   | Horní propoust | -3 dB | -46dB | 450  | 500  | - | - 
| 9   | Horní propoust | -3 dB | -46dB | 750  | 800  | - | - 
| 10  | Horní propoust | -3 dB | -46dB | 900  | 1000 | - | - 
| 11  | Pásmová propoust | -3 dB | -46dB | 150  | 200 | 300 | 350
| 12  | Pásmová propoust | -3 dB | -46dB | 250  | 300 | 400 | 450
| 13  | Pásmová propoust | -3 dB | -46dB | 450  | 500 | 600 | 650
| 14  | Pásmová propoust | -3 dB | -46dB | 750  | 800 | 900 | 950
| 15  | Pásmová propoust | -3 dB | -46dB | 900  | 950 | 1050 | 1100
| 16  | Pásmová zádrž | -3 dB | -46dB | 150  | 200 | 300 | 350
| 17  | Pásmová zádrž | -3 dB | -46dB | 250  | 300 | 400 | 450
| 18  | Pásmová zádrž | -3 dB | -46dB | 450  | 500 | 600 | 650
| 19  | Pásmová zádrž | -3 dB | -46dB | 750  | 800 | 900 | 950
| 20  | Pásmová zádrž | -3 dB | -46dB | 900  | 950 | 1050 | 1100



