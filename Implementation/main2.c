#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h> 
#include "portaudio.h"
#include "fir.h"

#define NUM_SECONDS 6000
#define BUFF_LENGTH 2048	// nejsem si jistý zda bude stačit (frameCount v callbacku se liší podle platformy u mě je 383)
#define SAMPLE_RATE 16000
//---------------------------------------------------------------------------------------------------------------------------------------------------
float * in;
float * out;
sem_t in_wait;
sem_t out_wait;
unsigned long frameCnt;
//---------------------------------------------------------------------------------------------------------------------------------------------------
static int callback(const void *input, void *output, unsigned long frameCount, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData)
{
	sem_wait(&out_wait); 
	memcpy(in, input, frameCount);
	memcpy(output, out, frameCount);
	frameCnt = frameCount;
	sem_post(&in_wait); 
	return 0;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
void Process (void)
{
	while (1)
	{
		sem_wait(&in_wait);
		fir_process(in, out, frameCnt);
		sem_post(&out_wait); 
	}
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
void Init (void)
{
	in = (float*)malloc(BUFF_LENGTH * sizeof(float));
	out = (float*)calloc(sizeof(float), BUFF_LENGTH);
	sem_init(&in_wait, 1, 0); 
	sem_init(&out_wait, 1, 1);  
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
void DestroyBuffers (void)
{
	free(in);
	free(out);
	sem_destroy(&in_wait); 
	sem_destroy(&out_wait); 
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{   

	PaStream *stream;
	PaError err;

	Init();
	fir_init();

	Pa_Initialize();
	Pa_OpenDefaultStream(&stream, 1, 1, paFloat32, SAMPLE_RATE, 0, callback, NULL);
	Pa_StartStream(stream);
	Process();
	Pa_StopStream(stream);
	Pa_CloseStream(stream);
	Pa_Terminate();

	DestroyBuffers();
	fir_destroy();

	return 0;    
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
