#!/bin/bash

################################################################################
# Example run for extracting tones (blocking the others) from a chord C3-C4-C5.#
# (not really a chord, but three tones nonetheless). There must be generated   #
# files for filters 2, 11, 13, 16 and 5. Filter 2 extracts C3 (~130 Hz), fil-  #
# ter 11 extracts C4 (~261 Hz) and filter 13 extracts C5 (~523 Hz). Filter 16  #
# is a bonus, it extracts C3 and C5, blocking C4. Filter number 5 leaves all   #
# tones untouched so that we can hear the original sound                       #
#                                                                              #
# Input signal: C3 + C4 + C5
#                                                                              #
# CAUTION: Do not kill the script, or you will have to end the filtering proc- #
#          esses manually!                                                     #
#                                                                              #
################################################################################

amixer set Mic 10% >/dev/null

for i in 2_C3 11_C4 13_C5 16_C3C5 5_C3C4C5 ; do
    ./filter"$i" 2>/dev/null &
    last_pid=$!
    sleep 2
    kill $last_pid
done

