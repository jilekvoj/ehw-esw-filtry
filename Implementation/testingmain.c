/*

	This program does not take input from audio card but generates it itself.
	Trere are three generated signals:
		sine wave with frequency 100 Hz
		sine wave with frequency 150 Hz
		sum of sine wave with frequency 100 Hz and sine wave with frequency 150 Hz
	One of these signals is processed via fir filter and final signal is outputted to audio card.

*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "portaudio.h"
#include "fir.h"

#define NUM_SECONDS 6000
#define SAMPLE_RATE 16000

#define FSIN1 100
#define FSIN2 150

int start;
//---------------------------------------------------------------------------------------------------------------------------------------------------
static int callback(const void *input, void *output, unsigned long frameCount, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData)
{
	static double pos;
	if (start) {pos = 0; start = 0;}
	float * out = (float*)output;
	float in[frameCount];
	int i;
	
	for (i = 0; i < frameCount; i++)
	{
		// choose one signal:
		//in[i] = (127.0 * sin( (double) FSIN1 * (pos / (double)SAMPLE_RATE) * M_PI * 2. ));
		//in[i] = (127.0 * sin( (double) FSIN2 * (pos / (double)SAMPLE_RATE) * M_PI * 2. ));
		in[i] = (127.0 * sin( (double) FSIN1 * (pos / (double)SAMPLE_RATE) * M_PI * 2. )) + (127.0 * sin( (double) FSIN2 * (pos / (double)SAMPLE_RATE) * M_PI * 2. ));
		pos++;
		if (pos >= SAMPLE_RATE) pos -= SAMPLE_RATE;
	}
	fir_process(in, output, frameCount);
	return 0;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{   
	start = 1;
	fir_init();
	
	PaStream *stream;
	PaError err;
	Pa_Initialize();	
	Pa_OpenDefaultStream(&stream, 0, 1, paFloat32, SAMPLE_RATE, 0, callback, NULL);
	Pa_StartStream(stream);
	Pa_Sleep(NUM_SECONDS * 1000);
	Pa_StopStream(stream);
	Pa_CloseStream(stream);
	Pa_Terminate();

	fir_destroy();

	return 0;    
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
