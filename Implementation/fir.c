#include "fir.h"

// filter selection (XXk_YY.h -> XX = {16, 22, 41}; YY = <1, 20>)
#include "coefficients/16k_1.h"

static float * samples;

//---------------------------------------------------------------------------------------------------------------------------------------------------
void fir_init(void)
{
	samples = (float*)calloc(sizeof(float), BUF_SIZE);
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
void fir_process(const void* input, void *output, unsigned long numFrames)
{
	float acc;
	const float *in = (float*)input;
	float *out = (float *)output;
	int i, j;

	// put the new samples at end of the buffer
	memcpy(samples + BL - 1, input, sizeof(float) * numFrames);

	// convolve input samples with filter coefficients
	for (i = 0; i < numFrames; i++)
	{
		acc = 0;
		in = samples + BL - 1 + i;

		for (j = 0; j < BL; j++)
			acc += B[j] * (*in--);

		out[i] = acc;
	}

	// shift input samples back in time for next time
	memmove(samples, samples + numFrames, sizeof(float) * BL);
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
void fir_destroy(void)
{
	free (samples);
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
