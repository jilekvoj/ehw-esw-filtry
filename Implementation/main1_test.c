#include <stdio.h>
#include <string.h>
#include <math.h>
#include "portaudio.h"
#include "fir.h"

#define NUM_SECONDS 5
#define SAMPLE_RATE 16000

float * datain;
float * dataout;
int posin, posout;
//---------------------------------------------------------------------------------------------------------------------------------------------------
static int callback(const void *input, void *output, unsigned long frameCount, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData)
{
	memcpy(datain + posin, input, frameCount);
	fir_process(input, output, frameCount);
	memcpy(dataout + posout, output, frameCount);
	posin += frameCount;
	posout += frameCount;
	return 0;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{   
    int i;
    datain = (float*)malloc(sizeof(float) * SAMPLE_RATE * (NUM_SECONDS + 1));
    dataout = (float*)malloc(sizeof(float) * SAMPLE_RATE * (NUM_SECONDS + 1));
    posin = 0;
    posout = 0;

    PaStream *stream;
    PaError err;
	
    fir_init();

    Pa_Initialize();
    Pa_OpenDefaultStream(&stream, 1, 1, paFloat32, SAMPLE_RATE, 0, callback, NULL);
    Pa_StartStream(stream);
    Pa_Sleep(NUM_SECONDS * 1000);
    Pa_StopStream(stream);
    Pa_CloseStream(stream);
    Pa_Terminate();

    fir_destroy();

    for (i = 0; i < posin; i++)
    {
        printf("%f\n", datain[i]);
    }
    printf("\n\n");
    for (i = 0; i < posout; i++)
    {
        printf("%f\n", dataout[i]);
    }
    free(datain);
    free(dataout);
    return 0;    
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
