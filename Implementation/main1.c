#include "portaudio.h"
#include "fir.h"

#define NUM_SECONDS 6000
#define SAMPLE_RATE 16000
//---------------------------------------------------------------------------------------------------------------------------------------------------
static int callback(const void *input, void *output, unsigned long frameCount, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData)
{
	fir_process(input, output, frameCount);
	return 0;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{

    PaStream *stream;

    fir_init();

    Pa_Initialize();
    Pa_OpenDefaultStream(&stream, 1, 1, paFloat32, SAMPLE_RATE, 0, callback, NULL);
    Pa_StartStream(stream);
    Pa_Sleep(NUM_SECONDS * 1000);
    Pa_StopStream(stream);
    Pa_CloseStream(stream);
    Pa_Terminate();

    fir_destroy();

    return 0;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
