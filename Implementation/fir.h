#ifndef __FIR_FILT_H__
#define __FIR_FILT_H__

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define BUF_SIZE 3000

void fir_init(void);
void fir_process(const void* input, void *output, unsigned long numFrames);
void fir_destroy(void);

#endif
