#include <stdio.h>
#include <string.h>
#include <math.h>
#include "portaudio.h"
#include "fir.h"

#define NUM_SECONDS 60

//takto ohraničené části jsem přidal já
//-------------------------------------
#define FSIN1 20
#define FSIN2 30
#define FSIN FSIN1 * FSIN2
int start;
float data[FSIN];
//-------------------------------------

static int callback(const void *input, void *output, unsigned long frameCount, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData)
{
	//-------------------------------------
	static int pos;
	if (start) {pos = 0; start = 0;}
	float * out = (float*)output;
	
	for (int i = 0; i < frameCount; i++)
	{
		out[i] = data[pos++];
		if (pos >= FSIN) pos -= FSIN;
	}
    return 0; 
    //-------------------------------------
    //return fir_process(input, output, frameCount, 0);
}

int main(int argc, char *argv[])
{   
	//-------------------------------------
	for (int i = 0; i < FSIN; i++)
	{
		data[i] = (127.0 * sin( ((double)i/(double)FSIN1) * M_PI * 2. )) + (127.0 * sin( ((double)i/(double)FSIN2) * M_PI * 2. ));
		printf("%lf\n", data[i]);
	}
	start = 1;
	//-------------------------------------

    PaStream *stream;
    PaError err;
	
    fir_init();

    Pa_Initialize();
    //-------------------------------------
    Pa_OpenDefaultStream(&stream, 0, 1, paFloat32, 44100, 256, callback, NULL);
    //-------------------------------------
    //Pa_OpenDefaultStream(&stream, 1, 1, paFloat32, 44100, 256, callback, NULL);
    Pa_StartStream(stream);
    Pa_Sleep(NUM_SECONDS * 1000);
    Pa_StopStream(stream);
    Pa_CloseStream(stream);
    Pa_Terminate();

    fir_destroy();

    return 0;    
}

