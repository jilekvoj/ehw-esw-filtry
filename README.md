# NI-ESW: Návrh a implementace audio filtrů pro Raspberry PI

## Soubory
* [Zadání a dokumentace](./Dokumentace.pdf)
* Samotná implementace - ve složce [Implementation](./Implementation)
* [Report testování](./testovani_export.pdf)
* Ukázka
  * Popis v souboru [demo.bash](./Implementation/demo/demo.bash)
  * [Nahrávka ukázky](./Implementation/demo/recording.m4a)
* Prezentace
  * [Progress během semestru](./prezentace_progres_export.pdf)
  * [Závěrečná prezentace](./finalni_prezentace.pdf)

## Návod ke kompilaci a spuštení
* Nainstalujte knihovnu PortAudio, viz [zde](http://files.portaudio.com/docs/v19-doxydocs/tutorial_start.html)
* V souboru [Implementation/fir.c](./Implementation/fir.c) změnte řádek číslo 4 dle požadovaného filtru
* V souboru [Implementation/main1.c](./Implementation/main1.c) změňte řádek číslo 5 dle požadované Fs
* Příkazem `make` zkompilujte program
* Spuštěním `./filter` spusťte program

## Autoři
* Vojtěch Jílek - team leader, návrh filtrů, implementace, dokumentace
* Karel Hevessy - návrh filtrů, měření, dokumentace
